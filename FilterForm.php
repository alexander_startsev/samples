<?php
namespace api\models;

use console\models\Yacht;
use yii\db\Query;

class FilterForm
{
    /**
     * @param array $params - query params
     * @return array - actual data for search form
     */
    public function getFilterData(array $params) : array
    {
        $countries = $this->getCountries($params);
        $charter_types = $this->getParams('servicetype', $params);
        $yacht_types = $this->getParams('kind', $params);
        $shipyards = $this->getParams('shipyard', $params, false);
        $length_range = $this->getParamRange('length');
        $cabins_range = $this->getParamRange('cabins');
        $price_range = $this->getPriceRange();

        return array(
            'countries' => $countries,
            'charter_types' => $charter_types,
            'yacht_types' => $yacht_types,
            'shipyards' => $shipyards,
            'length_range' => $length_range,
            'cabins_range' => $cabins_range,
            'price_range' => $price_range
        );
    }


    /**
     * @param array $params - query params
     * @return array - countries with array of bases for each
     */
    public function getCountries(array $params) : array
    {
        $query = new Query();
        $countries = $query
            ->select([
                    'base.country',
                    'translation.country AS translation',
                    'string_agg(DISTINCT base.name, \',\' ORDER BY base.name) AS ports'
            ])
            ->from('base')
            ->leftJoin('base_translation translation', [
                'and',
                'base.id = translation.base_id',
                ['translation.lang' => $params['lang']]
            ])
            ->groupBy(['base.country', 'translation.country'])
            ->orderBy('translation.country')
            ->all();
        foreach($countries as $key => $country) {
            $countries[$key]['ports'] = explode(',', $country['ports']);
        }

        return $countries;
    }

    /**
     * @param string $param - query params
     * @param array $params - param name
     * @param bool $translation - code of translating language
     * @return array - unique values for param
     */
    private function getParams(string $param, array $params, bool $translation = true) : array
    {
        $query = new Query();
        $query
            ->select([
                'DISTINCT "param"."value" AS value',
            ])
            ->from('param')
            ->where(['name' => $param])
            ->orderBy($translation ? 'translation.value' : '"param"."value"');

        if($translation) {
            $query->addSelect('translation.value AS translation')
                ->innerJoin('param_translation translation', 'param.id = translation.param_id')
                ->andWhere(['lang' => $params['lang']]);
        }

        return $query->all();
    }

    /**
     * @param string $param - param name
     * @return array - min and max param value
     */
    private function getParamRange(string $param) : array
    {
        $query = new Query();
        $range = $query->select([
            'CEIL(MIN(CAST(value AS decimal))) AS min_value',
            'CEIL(MAX(CAST(value AS decimal))) AS max_value'
        ])
            ->from('param')
            ->where([
                'and',
                ['name' => $param],
                'value != \'\''
            ])
            ->all();

        return $range;
    }

    /**
     * @return array - min and max rent price per week
     */
    private function getPriceRange()
    {
        $query = new Query();
        $range = $query->select([
            'MIN(price) AS min_value',
            'MAX(price) AS max_value'
        ])
            ->from('rent_period')
            ->where(
                [
                    'and',
                    ['>=', 'rent_period.finish_date', date('Y-m-d')],
                ]
            )
            ->all();

        return $range;
    }
}