<?php

namespace api\models;

use console\models\Catalog;
use console\models\SpecialOffer;
use console\models\Yacht;
use DateTime;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Url;

class YachtSearch
{
    public function search(array $params)
    {
        $rent_period = strtotime($params['finish-date']) - strtotime($params['begin-date']);
        $begin_date = new DateTime($params['begin-date']);
        $finish_date = new DateTime($params['finish-date']);
        $rent_period_days = date_diff($finish_date, $begin_date)->days;
        $current_day = date('Y-m-d');
        $day_num = $this->getDayOfWeek($params['begin-date']);

        $prices_query = $this->preparePriceQuery($params['begin-date'], $params['finish-date']);
        $discounts_query = $this->prepareDiscountQuery($params['begin-date'], $params['finish-date'], $current_day, $rent_period);

        $general_query = $this->prepareGeneralSearchQuery($params['begin-date'], $params['finish-date'], $prices_query, $discounts_query, $day_num);
        $special_query = $this->prepareSpecialSearchQuery($params['begin-date'], $params['finish-date']);

        if($rent_period_days >= 7) {
            $query = $general_query;
            if(isset($params['min-price']) && isset($params['max-price'])) {
                $query->andFilterWhere(['between', '(SELECT prices.base_price - prices.base_price * COALESCE(actual_discounts.discount, (0)) / 100)', $params['min-price'], $params['max-price']]);
            }
        }
        else {
            $query = $special_query;
            if(isset($params['min-price']) && isset($params['max-price'])) {
                $query->andFilterWhere(['between', 'price', $params['min-price'], $params['max-price']]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $params['limit']
            ]
        ]);

        if(isset($params['country'])) {
            $query->andFilterWhere(['country' => $params['country']]);
        }
        if(isset($params['shipyard'])) {
            $query->andFilterWhere(['catalog.shipyard' => $params['shipyard']]);
        }
        if(isset($params['service-type'])) {
            $query->andFilterWhere(['catalog.service_type' => $params['service-type']]);
        }
        if(isset($params['yacht-type'])) {
            $query->andFilterWhere(['catalog.yacht_type' => $params['yacht-type']]);
        }
        if(isset($params['port-name'])) {
            $query->andFilterWhere(['like', 'base.name', $params['port-name']]);
        }
        if(isset($params['min-length']) && isset($params['max-length'])) {
            $query->andFilterWhere(['between', 'CAST(catalog.length AS decimal)', $params['min-length'], $params['max-length']]);
        }
        if(isset($params['min-cabins-count']) && isset($params['max-cabins-count'])) {
            $query->andFilterWhere(['between', 'CAST(catalog.cabins_count AS decimal)', $params['min-cabins-count'], $params['max-cabins-count']]);
        }
        if(isset($params['yacht-age'])) {
            $years = $this->getYachtYearBuildingPeriod($params['yacht-age']);
            $query->andFilterWhere(['between', 'catalog.created_year', $years['begin-year'], $years['finish-year']]);
        }

        return $dataProvider;
    }

    public function getYacht(int $id, array $params)
    {
        $rent_period = strtotime($params['finish-date']) - strtotime($params['begin-date']);
        $begin_date = new DateTime($params['begin-date']);
        $finish_date = new DateTime($params['finish-date']);
        $rent_period_days = date_diff($finish_date, $begin_date)->days;
        $current_day = date('Y-m-d');
        $prices_query = $this->preparePriceQuery($params['begin-date'], $params['finish-date'], $id);
        $discounts_query = $this->prepareDiscountQuery($params['begin-date'], $params['finish-date'], $current_day, $rent_period, $id);

        $yacht = Yacht::find()
            ->select([
                'yacht.id',
                'yacht.bm_yacht_id',
                'yacht.name',
                'yacht.model',
                'base_translation.country',
                'string_agg(DISTINCT base.name, \'/\') AS base',
                'json_agg(DISTINCT base.lat) AS lat',
                'json_agg(DISTINCT base.lng) AS lng',
                'check_in_day',
                '@(DATE_PART(\'day\', \''. $params['finish-date'] .'\'::date) - DATE_PART(\'day\', \'' . $params['begin-date'] . '\'::date)) AS date_interval',
                'base_price.base_price',
                'COALESCE(actual_discounts.discount, 0) AS discount',
                '(SELECT base_price.base_price - base_price.base_price * COALESCE(actual_discounts.discount, 0) / 100) AS actual_price',
                '(CASE WHEN ' . $rent_period_days . ' >= 7 
                 THEN CASE WHEN "rent"."id" IS NULL THEN 1 ELSE 0 END
                 ELSE CASE WHEN "special_offer"."id" IS NULL THEN 1 ELSE 0 END
                 END
                ) AS current_status'
            ])
            ->leftJoin($prices_query . ' AS base_price', 'base_price.yacht_id = yacht.id')
            ->leftJoin($discounts_query . ' AS actual_discounts', 'actual_discounts.yacht_id = yacht.id')
            ->leftJoin('param max_discounts', [
                'and',
                'yacht.id = max_discounts.yacht_id',
                ['max_discounts.name' => 'maximumdiscount']
            ])
            ->leftJoin('location', 'yacht.id = location.yacht_id')
            ->leftJoin('base', 'base.id = (CASE WHEN location.base_id IS NULL THEN yacht.base_id ELSE location.base_id END)')
            ->leftJoin('base_translation', [
                'and',
                'base_translation.base_id = base.id',
                ['base_translation.lang' => $params['lang']]
            ])
            ->leftJoin('rent', [
                'and',
                'rent.yacht_id = yacht.id',
                ['<=', 'rent.begin_date', $params['begin-date']],
                ['>=', 'rent.finish_date', $params['finish-date']]
            ])
            ->leftJoin('special_offer', [
                'and',
                'special_offer.yacht_id = yacht.id',
                ['=', 'special_offer.begin_date', $params['begin-date']],
                ['=', 'special_offer.finish_date', $params['finish-date']]
            ])
            ->with([
                'prices' => function(ActiveQuery $query) {
                    $query->select(['yacht_id', 'price', 'begin_date', 'finish_date'])
                        ->where(['>=', 'finish_date', date('Y-m-d')])
                        ->orderBy('finish_date');
                },
                'equipments' => function(ActiveQuery $query) {
                    $params = Yii::$app->request->queryParams;
                    $query->select(
                        [
                            'yacht_id',
                            'equipment_translation.category',
                            'string_agg(DISTINCT equipment_translation.name, \', \') AS equipments'
                        ])
                        ->leftJoin('equipment_translation', [
                            'and',
                            'equipment_translation.equipment_id = equipment.id',
                            ['lang' => $params['lang']]
                        ])
                        ->groupBy(['yacht_id', 'equipment_translation.category']);
                },
                'images' => function(ActiveQuery $query) {
                    $query->select(
                        [
                            'yacht_id',
                            'CONCAT(\'' . Url::to(['images/'], true) . '\', \'/\', path) AS url',
                            'alt'
                        ]);
                },
                'params' =>function(ActiveQuery $query) {
                    $params = Yii::$app->request->queryParams;
                    $query->select(['yacht_id', 'name', 'param_translation.value'])
                        ->leftJoin('param_translation', [
                            'and',
                            'param_translation.param_id = param.id',
                            ['lang' => $params['lang']]
                        ])
                        ->indexBy('name');
                },
                'services' => function(ActiveQuery $query) {
                    $params = Yii::$app->request->queryParams;
                    $query->select([
                        'service.yacht_id',
                        'service_translation.name',
                        'service.price',
                        'service.time_period',
                        'service.per_person',
                        'service.obligatory',
                        'service.include_in_yacht_price',
                        'service.begin_date',
                        'service.finish_date'
                    ])
                        ->leftJoin('service_translation', [
                            'and',
                            'service_translation.service_id = service.id',
                            ['lang' => $params['lang']]
                        ])
                        ->leftJoin('yacht', 'yacht.id = service.yacht_id')
                        ->where([
                            'and',
                            ['>=','service.finish_date', date('Y-m-d')],
                            [
                                'or',
                                'service.base_id = 0',
                                'service.base_id = yacht.base_id'
                            ]
                        ])
                        ->orderBy('service.obligatory DESC');
                }
            ])
            ->where([
                'and',
                ['yacht.id' => $id],
            ])
            ->groupBy([
                'yacht.id',
                'yacht.bm_yacht_id',
                'yacht.name',
                'yacht.model',
                'base_translation.country',
                'check_in_day',
                'base_price.base_price',
                'actual_discounts.discount',
                'actual_price',
                'current_status'
            ])
            ->asArray()
            ->all();

        return  $yacht;
    }

    /**
     * Query to execute when selected period 8 days or more.
     * @param string $begin_date - begin date of rent period
     * @param string $finish_date - end date of rent period
     * @param string $prices_query - prepared price query
     * @param $discounts_query - prepared discount query
     * @param int $day_num - day number of week for begin date of rent period
     * @return ActiveQuery
     */
    private function prepareGeneralSearchQuery(
        string $begin_date,
        string $finish_date,
        string $prices_query,
        $discounts_query,
        int $day_num
    ) : ActiveQuery
    {
        $query_1 = Catalog::find()
            ->select([
                'catalog.id',
                'catalog.yacht_id',
                'catalog.name',
                'catalog.model',
                'base.country',
                'string_agg(DISTINCT base.name, \'/\') AS base',
                'CEIL(COALESCE(prices.base_price, 0)) AS base_price',
                'COALESCE(actual_discounts.discount, 0) AS discount',
                'CEIL((SELECT prices.base_price - prices.base_price * COALESCE(actual_discounts.discount, 0) / 100)) AS price',
                'catalog.shipyard',
                'catalog.service_type',
                'catalog.yacht_type',
                'catalog.length',
                'catalog.cabins_count',
                'catalog.created_year',
                'catalog.berths',
                'CONCAT(\'' . Url::to(['images/'], true) . '/\', catalog.image) AS image'
            ])
            ->leftJoin('location', [
                'and',
                'catalog.yacht_id = location.yacht_id',
                '\'' . $begin_date . '\' BETWEEN location.begin_date AND location.finish_date'
            ])
            ->leftJoin('base', 'base.id = (CASE WHEN location.base_id IS NULL THEN catalog.base_id ELSE location.base_id END)')
            ->leftJoin($prices_query . 'AS prices', 'prices.yacht_id = catalog.yacht_id')
            ->leftJoin($discounts_query . 'AS actual_discounts', 'actual_discounts.yacht_id = catalog.yacht_id')
            ->innerJoin('rent', 'rent.yacht_id = catalog.yacht_id')
            ->where([
                'and',
                ['<=', 'rent.begin_date', $begin_date],
                ['>=', 'rent.finish_date', $finish_date],
                [
                    'or',
                    ['catalog.check_in_day' => $day_num],
                    ['catalog.check_in_day' => -1]
                ]
            ])
            ->groupBy([
                'catalog.yacht_id',
                'catalog.name',
                'catalog.model',
                'base.country',
                'prices.base_price',
                'actual_discounts.discount',
                'catalog.shipyard',
                'catalog.service_type',
                'catalog.yacht_type',
                'catalog.length',
                'catalog.cabins_count',
                'catalog.created_year',
                'catalog.berths',
                'image',
                'catalog.id',
                'catalog.check_in_day'
            ])
            ->orderBy('"prices"."base_price" = 0, (SELECT prices.base_price - prices.base_price * COALESCE(actual_discounts.discount, (0)) / 100) ASC')
            ->asArray();

        return $query_1;
    }

    /**
     * Query to execute when selected period 7 days or less.
     * @param string $begin_date - begin date of rent period
     * @param string $finish_date - end date of rent period
     * @return ActiveQuery
     */
    private function prepareSpecialSearchQuery(string $begin_date, string $finish_date) : ActiveQuery
    {
        $query_2 = SpecialOffer::find()
            ->select([
                'catalog.id',
                'catalog.yacht_id',
                'catalog.name',
                'catalog.model',
                'base.country',
                'base.name AS base',
                'special_offer.base_price',
                'special_offer.discount',
                'special_offer.price',
                'catalog.shipyard',
                'catalog.service_type',
                'catalog.yacht_type',
                'catalog.length',
                'catalog.cabins_count',
                'catalog.created_year',
                'catalog.berths',
                'CONCAT(\'' . Url::to(['images/'], true) . '/\', catalog.image) AS image',
            ])
            ->leftJoin('catalog', 'catalog.yacht_id = special_offer.yacht_id')
            ->leftJoin('base', 'base.id = special_offer.base_id')
            ->where([
                'and',
                ['<=', 'begin_date', $begin_date],
                ['>=', 'finish_date', $finish_date]
            ])
            ->orderBy('"price" = 0, price ASC')
            ->asArray();

        return $query_2;
    }

    /**
     * Calculate rent price for each yacht in selected period.
     * @param string $begin_date - begin date of rent period
     * @param string $finish_date - end date of rent period
     * @param int|null $id - yacht id
     * @return string - prepared query for select yachts' prices
     */
    private function preparePriceQuery(string $begin_date, string $finish_date, int $id = null) : string
    {
        $prices_query = '(SELECT 
	rent_period.yacht_id,
	SUM(
		CASE
		WHEN rent_period.finish_date < \'' . $finish_date .'\' AND rent_period.begin_date <= \'' . $begin_date . '\' 
		THEN (rent_period.finish_date - \'' . $begin_date . '\'::date) * rent_period.price / 7
		WHEN rent_period.finish_date >= \'' . $finish_date . '\' AND rent_period.begin_date > \'' . $begin_date . '\' 
		THEN (\'' . $finish_date . '\'::date - rent_period.begin_date) * rent_period.price / 7
		WHEN rent_period.finish_date < \'' . $finish_date .'\' AND rent_period.begin_date > \'' . $begin_date . '\' 
		THEN (rent_period.finish_date - rent_period.begin_date) * rent_period.price / 7
		WHEN rent_period.finish_date >= \'' . $finish_date . '\' AND rent_period.begin_date <= \'' . $begin_date . '\' 
		THEN (\'' . $finish_date .'\'::date - \'' . $begin_date .'\'::date) * rent_period.price / 7
		ELSE rent_period.price
		END) AS base_price
        FROM rent_period
        INNER JOIN (
            SELECT rent_period.yacht_id, rent_period.begin_date
            FROM rent_period
            WHERE \'' . $begin_date . '\' >= rent_period.begin_date AND rent_period.finish_date > \'' . $begin_date . '\'
        ) AS begin_periods
        ON rent_period.yacht_id = begin_periods.yacht_id
        INNER JOIN (
            SELECT rent_period.yacht_id, rent_period.finish_date
            FROM rent_period
            WHERE \'' . $finish_date . '\' > rent_period.begin_date AND rent_period.finish_date >= \'' . $finish_date . '\'
        ) AS finish_periods
        ON rent_period.yacht_id = finish_periods.yacht_id
        WHERE rent_period.begin_date BETWEEN begin_periods.begin_date AND finish_periods.finish_date
        AND rent_period.finish_date BETWEEN begin_periods.begin_date AND finish_periods.finish_date ';
        if($id) {
            $prices_query .= 'AND rent_period.yacht_id = ' . $id . ' ';
        }
        $prices_query .= 'GROUP BY rent_period.yacht_id)';

        return $prices_query;
    }

    /**
     * Calculate discount for each yacht in selected period.
     * @param string $begin_date - begin date of rent period
     * @param string $finish_date - end date of rent period
     * @param string $current_day - today's date
     * @param int $rent_period - time of rent period
     * @param int|null $id - yacht id
     * @return string - prepared query for select yachts' discounts
     */
    private function prepareDiscountQuery(string $begin_date, string $finish_date, string $current_day, int $rent_period, int $id = null) : string
    {
        $discounts_query = '(SELECT
        filtered_discounts_2.yacht_id,
        filtered_discounts_2.value AS discount
        FROM(
            SELECT 
            filtered_discounts_1.yacht_id, 
            filtered_discounts_1.bm_id, 
            filtered_discounts_1.value, 
            filtered_discounts_1.affected_by_maximum, 
            filtered_discounts_1.max_discount, 
            filtered_discounts_1.sum_exclude_others,
            filtered_discounts_1.count_max_ex_1,
            filtered_discounts_1.count_max_ex_0,
            MAX(filtered_discounts_1.bm_id) OVER(PARTITION BY filtered_discounts_1.yacht_id ORDER BY filtered_discounts_1.yacht_id) AS max_bm_id
            FROM(
                SELECT
                counted_discounts.yacht_id,
                counted_discounts.bm_id,
                counted_discounts.value,
                counted_discounts.affected_by_maximum,
                counted_discounts.exclude_others,
                counted_discounts.max_discount,
                counted_discounts.sum_exclude_others,
                counted_discounts.max_discount_ex_1,
                counted_discounts.max_discount_ex_0,
                COUNT(*) FILTER(WHERE counted_discounts.affected_by_maximum = 1 AND counted_discounts.value = counted_discounts.max_discount_ex_1 AND counted_discounts.exclude_others = 1)
                OVER w2 AS count_max_ex_1,
                COUNT(*) FILTER(WHERE counted_discounts.affected_by_maximum = 1 AND counted_discounts.value = counted_discounts.max_discount_ex_0 AND counted_discounts.exclude_others = 0)
                OVER w2 AS count_max_ex_0
                FROM(
                    SELECT
                    discount.yacht_id,
                    discount.bm_id, 
                    discount.value, 
                    discount.affected_by_maximum, 
                    discount.exclude_others,
                    catalog.max_discount,
                    SUM(discount.exclude_others) OVER w1 AS sum_exclude_others,
                    SUM(discount.affected_by_maximum) FILTER(WHERE discount.exclude_others = 1) OVER w1 AS sum_affected_by_maximum_1,
                    SUM(discount.affected_by_maximum) FILTER(WHERE discount.exclude_others = 0) OVER w1 AS sum_affected_by_maximum_0,
                    MAX(discount.value) FILTER(WHERE discount.exclude_others = 1) OVER w1 AS max_discount_ex_1,
                    MAX(discount.value) FILTER(WHERE discount.exclude_others = 0) OVER w1 AS max_discount_ex_0
                    FROM discount
                    INNER JOIN catalog
                    ON discount.yacht_id = catalog.yacht_id
                    WHERE \'' . $begin_date . '\' BETWEEN sailing_begin_date AND sailing_finish_date
                    AND CURRENT_DATE BETWEEN valid_begin_date AND valid_finish_date
                    AND included_in_yacht_price = 0 
                    AND time_from <= (CASE WHEN type = 0 THEN ' . $rent_period . ' WHEN type = 3 THEN ' . $rent_period . ' ELSE time_from END) 
                    AND time_to >= (CASE WHEN type = 0 THEN ' . $rent_period . ' WHEN type = 3 THEN ' . $rent_period . ' ELSE time_from END)
                    
                    WINDOW w1 AS (PARTITION BY discount.yacht_id ORDER BY discount.exclude_others DESC)
                ) AS counted_discounts
                WINDOW w2 AS (PARTITION BY counted_discounts.yacht_id ORDER BY counted_discounts.exclude_others DESC)
            ) AS filtered_discounts_1
            WHERE (CASE WHEN filtered_discounts_1.sum_exclude_others > 0 
                   THEN filtered_discounts_1.exclude_others = 1 
                   ELSE filtered_discounts_1.exclude_others = 0 
                   END)
            AND (
                CASE WHEN filtered_discounts_1.sum_exclude_others > 0
                THEN CASE WHEN filtered_discounts_1.count_max_ex_1 > 0 
                THEN filtered_discounts_1.value = filtered_discounts_1.max_discount_ex_1
                AND filtered_discounts_1.value < filtered_discounts_1.max_discount
                ELSE filtered_discounts_1.affected_by_maximum = 0
                END
                ELSE CASE WHEN filtered_discounts_1.count_max_ex_0 > 0
                THEN filtered_discounts_1.value = filtered_discounts_1.max_discount_ex_0
                AND filtered_discounts_1.value < filtered_discounts_1.max_discount
                ELSE filtered_discounts_1.affected_by_maximum = 0
                END
                END
            )
        ) AS filtered_discounts_2
        WHERE filtered_discounts_2.bm_id = filtered_discounts_2.max_bm_id ';
        if($id) {
            $discounts_query .= 'AND filtered_discounts_2.yacht_id =' . $id . ' ';
        }
        $discounts_query .= ')';
        return $discounts_query;
    }

    /**
     * Return day number of week
     * @param string $date - first day of rent period
     * @return int - day number of week (from 1 to 7)
     */
    private function getDayOfWeek(string $date) : int
    {
        $temp_day_num = date('w', strtotime($date));
        $day_num = $temp_day_num + 1;
        return $day_num;
    }

    /**
     * Chose years' period when yacht could be built
     * @param string $period - name of period
     * @return array - begin and finish years
     */
    private function getYachtYearBuildingPeriod(string $period) : array
    {
        $current_date = new DateTime();
        $_begin_date = new DateTime();
        $_finish_date = new DateTime();
        $current_year = $current_date->format('Y');
        $years = array();
        switch($period) {
            case 'period-1' :
                $begin_date = $_begin_date->modify('- 2 years');
                $years = array(
                    'begin-year' => $begin_date->format('Y'),
                    'finish-year' => $current_year
                );
                break;
            case 'period-2' :
                $begin_date = $_begin_date->modify('- 5 years');
                $finish_date = $_finish_date->modify('- 2 years');
                $years = array(
                    'begin-year' => $begin_date->format('Y'),
                    'finish-year' => $finish_date->format('Y')
                );
                break;
            case 'period-3' :
                $begin_date = $_begin_date->modify('- 10 years');
                $finish_date = $_finish_date->modify('- 5 years');
                $years = array(
                    'begin-year' => $begin_date->format('Y'),
                    'finish-year' => $finish_date->format('Y')
                );
                break;
            case 'period-4' :
                $finish_date = $_finish_date->modify('- 10 years');
                $years = array(
                    'begin-year' => '1900',
                    'finish-year' => $finish_date->format('Y')
                );
                break;
            default :
                $years = array(
                    'begin-year' => '1900',
                    'finish-year' => $current_year
                );
        }

        return $years;
    }
}