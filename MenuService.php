<?php

namespace Bagistoex\MenuBuilder\Services;

use Bagistoex\MenuBuilder\Models\Menu;
use Webkul\Core\Models\Locale;
use Webkul\Product\Models\ProductFlat;

class MenuService
{
    public static function getMenus()
    {
        $rows = self::selectMenu();

        $menus = array();
        foreach($rows as $row) {
            $items = self::prepareItems($row['items']);
            $items = self::buildTree($items);

            $menus[] = array(
                'menu_type' => $row['menu_type'],
                'columns' => $items
            );
        }

        return $menus;
    }

    public static function buildTree(array $items)
    {
        $rootItems = self::getRoots($items);
        self::getChildren($rootItems, $items);

//        uasort($rootItems, array(self::class, 'menuItemsSort'));
//        self::sortMenuItems($rootItems);
        $rootItems = self::array_values_recursive($rootItems);
        $rootItems = array_values($rootItems);

        return $rootItems;
    }

    protected static function selectMenu()
    {
        $locale = request()->get('locale') ?: app()->getLocale();
        $locale_id = Locale::where('code', $locale)->first()->id;

        return Menu::with(['items' => function($query) use($locale_id) {
            $query->where([
                ['status', 1],
                ['locale_id', $locale_id]
            ]);
        }])
            ->select('id', 'code as menu_type')
            ->get()
            ->toArray();
    }

    protected static function prepareItems($rows)
    {
        $items = array();
        foreach($rows as $row) {
            $items[] = array(
                'id' => $row['id'],
                'parent_id' => $row['parent_id'],
                'name' => $row['title'],
                'position' => $row['position'],
                'slug' => self::getSlug($row['item_type'], $row['item_id'])
            );
        }

        return $items;
    }

    protected static function getSlug($class, $id)
    {
        if ($class == 'group') return '';

        $meta = call_user_func( $class . '::find', $id);

        if (!empty($meta['url_key'])) {
            return $meta['url_key'];
        } elseif (!empty($meta['slug'])) {
            return $meta['slug'];
        }

        return '';
    }

    protected static function getRoots(array $items)
    {
        $roots = array();

        foreach($items as $item) {
            if($item['parent_id'] == 0) {
                $item['items'] = array();
                $roots[$item['id']] = $item;
            }
        }

        return $roots;
    }

    protected static function getChildren(array &$roots, array $items)
    {
        foreach($items as $id =>$item) {
            if (!isset($item['items'])) {
                $item['items'] = array();
            }

            if(isset($roots[$item['parent_id']])) {
                $roots[$item['parent_id']]['items'][$item['id']] = $item;
                unset($items[$id]);
            }
        }

        foreach($roots as $id => $root) {
            if(isset($root['items'])) {
                self::getChildren($roots[$id]['items'], $items);
            }
        }
    }

    protected static function sortMenuItems(array &$items)
    {
        if (is_array($items)) {
            foreach ($items as $key => $item) {
                if (isset($item['items'])) {
                    $children = $item['items'];
                    uasort($children, array(self::class, 'menuItemsSort'));
                    $items[$key]['items'] = $children;
                    self::sortMenuItems($items[$key]['items']);
                }
            }
        }
    }

    protected static function menuItemsSort($x, $y)
    {
        if((int)$x['position'] > (int)$y['position']) {
            return true;
        } else if((int)$x['position'] < (int)$y['position']) {
            return false;
        } else {
            return 0;
        }
    }

    protected static function array_values_recursive($arr)
    {
        foreach ($arr as $key => $value)
        {
            if (is_array($value))
            {
                $arr[$key] = self::array_values_recursive($value);
            }
        }

        if (isset($arr['items']))
        {
            $arr['items'] = array_values($arr['items']);
        }

        return $arr;
    }
}
