<?php

namespace Bagistoex\MenuBuilder\Http\Controllers;

use Bagistoex\MenuBuilder\Models\Menu;
use Bagistoex\MenuBuilder\Models\MenuItem;
use Bagistoex\MenuBuilder\Services\MenuService;
use Webkul\Category\Models\CategoryTranslation;
use Webkul\CMS\Models\CMS;
use Webkul\Core\Models\Locale;
use Webkul\Product\Models\ProductFlat;
use  Bagistoex\MenuBuilder\Http\Resources\Category as CategoryResource;
use  Bagistoex\MenuBuilder\Http\Resources\Product as ProductResource;
use  Bagistoex\MenuBuilder\Http\Resources\Page as PageResource;

class MenuItemController extends Controller
{
    public function index($id)
    {
        $menu = Menu::find($id);
        $locale = request()->get('locale') ?: app()->getLocale();
        $locale_id = Locale::where('code', $locale)->first()->id;

        $items = MenuItem::where('menu_items.menu_id', $menu->id)
            ->where('menu_items.locale_id', $locale_id)
            ->select(
                'menu_items.id AS id',
                'menu_items.item_type as type',
                'menu_items.item_id as type_id',
                'menu_items.parent_id as parent_id',
                'menu_items.position as position',
                'menu_items.status as status',
                'menu_items.title as name'
            )
            ->get()
            ->toArray();

        $items = MenuService::buildTree($items);


        return view('menu-builder::menu-items.index')->with([
            'menu' => $menu,
            'itemTypes' => $this->getItemTypes(),
            'locale' => $locale,
            'locale_id' => $locale_id,
            'items' => $items
        ]);
    }

    public function availableTypes()
    {
        $type = request()->get('type');
        $locale = request()->get('locale') ?: app()->getLocale();
        $function = 'get' . $type;

        return $this->$function($locale);
    }

    public function store($id) {
        $menu = Menu::find($id);
        $items = request()->get('items');
        $locale_id = (int)request()->get('locale_id');
        $menu->items()->where('locale_id', '=', $locale_id)->delete();

        if (!empty($items)) {
            foreach($items as $item) {
                $this->createItem($menu, $item, $locale_id);
            }
        }
    }

    protected function createItem($menu, $item, $locale_id, $parent_id = 0) {
        $newItem = $menu->items()->create([
            'item_type' => $item['type'],
            'item_id' => $item['type_id'] ?? 0,
            'parent_id' => $parent_id,
//            'position' => $item['position'],
            'status' => $item['status'],
            'title' => $item['name'],
            'locale_id' => $locale_id
        ]);

        if($newItem->item_type == 'group') {
            foreach($item['items'] as $item) {
                $this->createItem($menu, $item, $locale_id, $newItem->id);
            }
        }
    }

    protected function getCategories($locale)
    {
        $colection = CategoryTranslation::where('locale', '=', $locale)
            ->join('categories', 'categories.id', '=', 'category_translations.category_id')
            ->get();

        return CategoryResource::collection($colection);
    }

    protected function getProducts($locale)
    {
        $colection = ProductFlat::where('locale', '=', $locale)
            ->join('products', 'product_flat.product_id', '=', 'products.id')
            ->get();

        return ProductResource::collection($colection);
    }

    protected function getPages($locale)
    {
        $locale_id = Locale::where('code', $locale)->first()->id;
        return PageResource::collection(CMS::where('locale_id', $locale_id)->get());
    }

    protected function getItemTypes()
    {
        return array(
            [
                'title' => 'Pages',
                'type' => 'Pages'
            ],
            [
                'title' => 'Categories',
                'type' => 'Categories'
            ],
            [
                'title' => 'Products',
                'type' => 'Products'
            ]
        );
    }
}